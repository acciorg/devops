# DevOps

I've created a CentOs 7 VM on Vmware Workstation for this case. Before I begin to the case steps, I created an additional disk (10 GB) from the VMWare Workstations settings. Since we want our VM to be up to date, we use "yum update" command to update packages installed on the system after the installation of os is completed.  

`$ yum update -y`

**If there is any kernel related package installed, we must reboot our VM to start with the new kernel. Then, I create a user in my name and change its password. Then modify the /etc/sudoers file.**

```
$ useradd reyhan.gulcetin
$ passwd reyhan.gulcetin
$ echo "reyhan.gulcetin ALL=(ALL) ALL" >> /etc/sudoers
$ su - reyhan.gulcetin

```


**To check partitions on the system, I used "fdisk -l" command. We can see the additional 10 GB disk in the output below. **

`$ sudo fdisk -l`

```
Disk /dev/sdb: 10.7 GB, 10737418240 bytes, 20971520 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/sda: 21.5 GB, 21474836480 bytes, 41943040 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0x000b438e

   Device Boot      Start         End      Blocks   Id  System
/dev/sda1   *        2048     2099199     1048576   83  Linux
/dev/sda2         2099200    41943039    19921920   8e  Linux LVM

Disk /dev/mapper/centos-root: 18.2 GB, 18249416704 bytes, 35643392 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/mapper/centos-swap: 2147 MB, 2147483648 bytes, 4194304 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes

**Since we can see the new disk on the server, we can format the disk to create a new partition.**
```


`$ sudo fdisk /dev/sdb`

```
Welcome to fdisk (util-linux 2.23.2).

Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.

Device does not contain a recognized partition table
Building a new DOS disklabel with disk identifier 0x6793bc64.

Command (m for help): **p**

Disk /dev/sdb: 10.7 GB, 10737418240 bytes, 20971520 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0x6793bc64

   Device Boot      Start         End      Blocks   Id  System

Command (m for help): **n**
Partition type:
   p   primary (0 primary, 0 extended, 4 free)
   e   extended
Select (default p): **p**
Partition number (1-4, default 1):
First sector (2048-20971519, default 2048):
Using default value 2048
Last sector, +sectors or +size{K,M,G} (2048-20971519, default 20971519):
Using default value 20971519
Partition 1 of type Linux and of size 10 GiB is set

Command (m for help): **w**
The partition table has been altered!

Calling ioctl() to re-read partition table.
Syncing disks.
```
`sudo mkfs.ext4 /dev/sdb1`

To mount new disk to bootcamp directory.
```
$ sudo mkdir /bootcamp
$ sudo mount /dev/sdb1 /bootcamp/
$ sudo lsblk
```

```
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0   20G  0 disk
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0   19G  0 part
  ├─centos-root 253:0    0   17G  0 lvm  /
  └─centos-swap 253:1    0    2G  0 lvm  [SWAP]
sdb               8:16   0   10G  0 disk
└─sdb1            8:17   0   10G  0 part /bootcamp
sr0              11:0    1  973M  0 rom
```


We use the command below to write "merhaba trendyol" to bootcamp.txt under the new directory we just created /opt/bootcamp.

`$ sudo mkdir /opt/bootcamp && echo "merhaba trendyol" | sudo tee /opt/bootcamp/bootcamp.txt`

**Go home directory, find and move "bootcamp.txt" under /bootcamp directory.**
```
$ cd ~
$ sudo find / -type f -name 'bootcamp.txt' -exec mv -i '{}' /bootcamp \;
```

**Task 2**


$ ansible-playbook second_case.yml

I misunderstood the last part of the second case and could not finish it on time :(. So I share as the way I thought "completed". 

```
For this configuration http://ip-address:80/ --> goes to wordpress welcome page
                       http://ip-address:80/devops --> goes to a simple index.html and displays "Hoş Geldin Devops"
                       
```

In the second_case.yml, there are steps for installation of Docker, Docker Compose, and git; starting and enabling Docker, and stopping firewalld. I clone the files from my github repo because it was faster for my environment. Finally, I run the app with docker-compose command.
