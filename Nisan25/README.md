**TASK#1
DOCKER İLE COUCHBASE KURULUMU**


Altı node'lu bir yapı kuracağım için container'ları aşağıdaki gibi kaldırdım:

```
$ docker run -d --name db1 couchbase
$ docker run -d --name db2 couchbase
$ docker run -d --name db3 -p 8091-8094:8091-8094 -p 11210:11210 couchbase
$ docker run -d --name db4 couchbase
$ docker run -d --name db5 couchbase
$ docker run -d --name db6 couchbase
```
Diğer beş coucbase container'ı birbirinin aynısı olduğundan, bu şekilde ayrı ayrı çalıştırmak yerine Docker Swarm kullanarak tek satırda da yapabiliriz:

```
$ docker service create --replicas 5 --name couchbase_nodes couchbase
$ docker service create --name couchbase-cluster-node -p 8091-8094:8091-8094 -p 11210:11210 couchbase
```

Bu şekilde isimleri çok uzun tuttuğum için kullanım kolaylığı adına ilk örnekteki gibi devam edeceğim.

$ docker logs <container-name> ile container'ların çalışıp çalışmadığını kontrol edebiliriz. Örneğin db3 container'ı için bakacak olursak:

```
$ docker logs db3
Starting Couchbase Server -- Web UI available at http://<ip>:8091 and logs available in /opt/couchbase/var/lib/couchbase/logs
```
Cluster'ımızı oluşturduktan sonra diğer node'ları eklemek için IP adreslerine ihtiyacımız olacak, IP adreslerini bulmak için aşağıdaki komutu kullanabiliriz:

`$ docker inspect --format '{{ .NetworkSettings.IPAddress }}' <container-name>`


Daha sonra http://host-ip-address:8091 adresini kullanarak oluşturmuş olduğumuz "couchbase-first-node" servisine erişiyoruz. Arayüze giriş yaptıktan sonra "Setup New Cluster" butonuna tıklayarak oluşturacağımız yeni cluster için gerekli bilgileri giriyoruz. (Bu kısma ait ilk ekran görüntüsü couchbase.PNG dosyasında mevcut.) Bucket'ı kendim eklemek yerine Couchbase'in sunduğu "beer-sample" ve "travel-sample" Bucket'ları üzerinden gideceğim. 

Cluster'ımıza yeni node'lar eklemek için aşağıdaki komutu kullandım:

```
couchbase-cli server-add -c <db3-IPAddress>:8091 \
--username Administrator \
--password password \
--server-add <container-IPAddress> \
--server-add-username Administrator \
--server-add-password password \
```
Eğer işlem başarılı olduysa aşağıdaki gibi bir output görmemiz gerekiyor:
`SUCCESS: Server added`

Tüm container'ları ekledikten sonra server listesine aşağıdaki komutla bakabiliriz:

```
$ couchbase-cli server-list -c 172.17.0.7:8091 --username Administrator --password password
ns_1@172.17.0.10 172.17.0.10:8091 healthy inactiveAdded
ns_1@172.17.0.11 172.17.0.11:8091 healthy inactiveAdded
ns_1@172.17.0.12 172.17.0.12:8091 healthy inactiveAdded
ns_1@172.17.0.7 172.17.0.7:8091 healthy active
ns_1@172.17.0.8 172.17.0.8:8091 healthy inactiveAdded
ns_1@172.17.0.9 172.17.0.9:8091 healthy inactiveAdded
```

Node'ları cluster'a ekledikten sonra cluster'ımızın rebalance olmasını sağlamak için aşağıdaki komutu kullanabiliriz:

```
$ couchbase-cli rebalance -c cluster-ip-address:8091 \
--username Administrator \
--password password
```

İşlem başarılı olduysa aşağıdaki gibi bir çıktı almamız gerekmekte:

```
Rebalancing
Bucket: 02/02 (travel-sample)                                                                                                                0 docs remaining
[=====================================================                                                                
SUCCESS: Rebalance complete
```

Rebalance işlemi sonrası tekrar server listesine baktığımızda aşağıdaki çıktıyı görüyoruz:

```
couchbase-cli server-list -c 172.17.0.7:8091 --username Administrator --password password
ns_1@172.17.0.10 172.17.0.10:8091 healthy active
ns_1@172.17.0.11 172.17.0.11:8091 healthy active
ns_1@172.17.0.12 172.17.0.12:8091 healthy active
ns_1@172.17.0.7 172.17.0.7:8091 healthy active
ns_1@172.17.0.8 172.17.0.8:8091 healthy active
ns_1@172.17.0.9 172.17.0.9:8091 healthy active
```




**TASK#2**

**[Debian ve Ubuntu için]**

```
$ sudo apt-get install git-all python3-dev python3-pip python3-setuptools cmake build-essential

$ python3 -m pip install couchbase

$ sudo apt-get install ipython3 -y
```


Öncelikle Python SDK için dokümandan yararlanarak gerekli kurulumları yaptım. Burada yapacağım diğer işlemlerde de couchbase'in dokümanlarından faydalandım. Python SDK'dan sonra container'ları barındırdığım host'a ipython3 indirdim ve aşağıdaki satırları o şekilde çalıştırdım.

```
# Tüm cluster bağlantı işlemleri için 
from couchbase.cluster import Cluster, ClusterOptions
from couchbase_core.cluster import PasswordAuthenticator

# SQL++ (N1QL) query desteği için
from couchbase.cluster import QueryOptions

# bu kısımda kendi cluster'ımızı belirtiyoruz, zaten host makinede olduğum için localhost olarak bıraktım.
cluster = Cluster('couchbase://localhost', ClusterOptions(
  PasswordAuthenticator('Administrator', 'password')))

# Ben de öncesinde travel-sample indirdiğim için örnekteki gibi travel-sample bucket'ı üzerinden gideceğim.
cb = cluster.bucket('travel-sample')
cb_coll = cb.default_collection()

```
JSON formatında, airline isminde örnek bir doküman tanımlıyoruz:

```
airline = {
  "type": "airline",
  "id": 1913,
  "callsign": "CBS",
  "iata": None,
  "icao": None,
  "name": "Sabiha Gökçen Havalimanı",
}
```

Bucket class'ını kullanarak dokümanlar için insert ve upsert (update&insert) gibi işlemleri yapabiliyoruz, buradaki örnekte upsert fonksiyonunu kullanacağız.

```
def upsert_document(doc):
  print("\nUpsert CAS: ")
  try:
    # key will equal: "airline_8091"
    key = doc["type"] + "_" + str(doc["id"])
    result = cb_coll.upsert(key, doc)
    print(result.cas)
  except Exception as e:
    print(e)
```

Yukarıda tanımlamış olduğumuz upsert_document fonksiyonunu, oluşturmuş olduğumuz airport dokümanıyla çalıştırıyoruz.

```
upsert_document(airline)

Upsert CAS:
1619641462292217856
```
Oluşturmuş olduğumuz dokümanı görüntülemek için key-value fonksiyonu çalıştırıyoruz.

```
def get_airline_by_key(key):
  print("\nGet Result: ")
  try:
    result = cb_coll.get(key)
    print(result.content_as[str])
  except Exception as e:
    print(e)
```

Airline dokümanı ve dokümanın içinde belirlediğimiz key ile birlikte fonksiyonu çalıştırıyoruz.

```
get_airline_by_key("airline_1913")

Get Result:
{'type': 'airline', 'id': 1913, 'callsign': 'CBS', 'iata': None, 'icao': None, 'name': 'Sabiha Gökçen Havalimanı'}
```
Dokümanı JSON formatında tekrar elde etmiş olduk. 

KAYNAK: https://docs.couchbase.com/python-sdk/current/hello-world/start-using-sdk.html



